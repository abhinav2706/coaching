package com.coaching.login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.coaching.common.Experience

class SignUpActivity(): AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up)
        findViewById<Button>(R.id.email_sign_in).setOnClickListener {  Experience.getExperience().appExperience?.openMainActivity(this)
            finish()
        }
    }
}