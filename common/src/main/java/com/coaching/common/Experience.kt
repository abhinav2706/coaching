package com.coaching.common

class Experience{

    companion object {
        private var instance: Experience= Experience()
        fun getExperience():Experience{
            if(instance == null){
                instance = Experience()
            }
            return instance
        }
    }
     var appExperience:AppExperience? =null

}