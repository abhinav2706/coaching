package com.coaching.coachingdemo.experience

import android.content.Context
import com.coaching.coachingdemo.MainActivity
import com.coaching.common.AppExperience

class AppExperienceImpl : AppExperience{
    override fun openMainActivity(context: Context) {
        MainActivity.openMainActivity(context)
    }

}