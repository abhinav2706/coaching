package com.coaching.coachingdemo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

class HeaderFragment:Fragment(){

    var photoNo:Int?=0
    companion object {
        var firebaseStorage = FirebaseStorage.getInstance().reference.child("Header Photos")
        fun getInstance(photoNo:Int):HeaderFragment{
            val bundle=Bundle()
            bundle.putInt("photo_no",photoNo)
            val headerFragment=HeaderFragment()
            headerFragment.setArguments(bundle)
            return headerFragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.header_fragment_layout,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        photoNo=arguments?.getInt("photo_no",0)
        if(photoNo!=0){
            firebaseStorage.child("photo" + photoNo.toString()+".jpg").downloadUrl.addOnSuccessListener { uri ->  Picasso.get().load(uri.toString()).resize(view.findViewById<ImageView>(R.id.imageView).width,view.findViewById<ImageView>(R.id.imageView).height).into(view.findViewById<ImageView>(R.id.imageView)) }

        }
    }
}