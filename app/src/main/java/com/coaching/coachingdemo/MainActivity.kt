package com.coaching.coachingdemo

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class MainActivity : AppCompatActivity() {

    lateinit var firebaseStorage:StorageReference
    lateinit var databaseRef:DatabaseReference
    companion object {
        fun openMainActivity(context: Context){
            val intent=Intent(context,MainActivity::class.java)
            context.startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        firebaseStorage = FirebaseStorage.getInstance().reference
        databaseRef = FirebaseDatabase.getInstance().getReference("Header Files Count")
        databaseRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(p0: DataSnapshot?) {
                if(p0!=null){
                    var headerViewPagerAdapter=HeaderViewPagerAdapter(supportFragmentManager,p0.value as Long,firebaseStorage)
                    var viewPager =findViewById<ViewPager>(R.id.header_view_pager)
                    viewPager.adapter = headerViewPagerAdapter
                    viewPager.offscreenPageLimit = 10
                }
            }

        })
    }
}
