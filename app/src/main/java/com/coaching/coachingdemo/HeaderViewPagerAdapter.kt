package com.coaching.coachingdemo

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.google.firebase.storage.StorageReference

class HeaderViewPagerAdapter(fm: FragmentManager, internal var pages: Long, var firebaseStorage: StorageReference) : FragmentStatePagerAdapter(fm){

    override fun getItem(position: Int): Fragment {
        return HeaderFragment.getInstance(position+1)
    }

    override fun getCount(): Int {
        return pages.toInt()
    }

}