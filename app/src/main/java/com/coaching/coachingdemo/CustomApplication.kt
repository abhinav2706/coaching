package com.coaching.coachingdemo

import android.app.Application
import com.coaching.coachingdemo.experience.AppExperienceImpl
import com.coaching.common.Experience

class CustomApplication: Application(){


    override fun onCreate() {
        super.onCreate()
        Experience.getExperience().appExperience = AppExperienceImpl()
    }
}